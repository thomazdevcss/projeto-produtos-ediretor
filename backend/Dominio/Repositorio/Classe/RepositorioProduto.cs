using System.Collections.Generic;
using backend.Dominio.Entidade;
using backend.Dominio.Repositorio.Interface;
using Microsoft.Extensions.Configuration;
using Dapper;
using System.Linq;
using System;

namespace backend.Dominio.Repositorio.Classe
{
    public class RepositorioProduto : IRepositorioProduto
    {
        private IConfiguration configuration;
        private IProvedorConexao provedorConexao;
        public RepositorioProduto(IConfiguration configuration, IProvedorConexao provedorConexao)
        {
            this.configuration = configuration;
            this.provedorConexao = provedorConexao;
        }
        /// <summary>
        /// Insere um novo produto no banco de dados.
        /// </summary>
        /// <param name="produto"></param>
        public void Inserir(Produto produto)
        {
            var param = new DynamicParameters();
            param.Add("@Nome", produto.Nome);
            param.Add("@Descricao", produto.Descricao);
            param.Add("@ImagemUrl", produto.Imagem);
            param.Add("@Valor", produto.Valor);

            var query = @"INSERT INTO produto VALUES
                          (@Nome,@Descricao,@Valor,@ImagemUrl)";

            using (var con = this.provedorConexao.ConectarSqlServer())
            {
                con.Query(query, param);
            }
        }
        /// <summary>
        /// Altera um unico produto, selecionado pelo id, do banco de dados.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="produto"></param>
        public void Alterar(int id, Produto produto)
        {
            var param = new DynamicParameters();
            param.Add("@Id", id);
            param.Add("@Nome", produto.Nome);
            param.Add("@Descricao", produto.Descricao);
            param.Add("@Imagem", produto.Imagem);
            param.Add("@Valor", produto.Valor);

            var query = @"UPDATE produto SET
                          nome = @Nome,
                          descricao = @Descricao,
                          valor = @Valor,
                          imagem = @Imagem
                          WHERE
                          id = @Id";

            using (var con = this.provedorConexao.ConectarSqlServer())
            {
                con.Query(query, param);
            }
        }
        /// <summary>
        /// Deleta um unico produto, selecionado pelo id, do banco de dados.
        /// </summary>
        /// <param name="id"></param>
        public void Deletar(int id)
        {
            var param = new DynamicParameters();
            param.Add("@Id", id);

            var query = @"DELETE FROM produto
                          WHERE id = @Id";

            using (var con = this.provedorConexao.ConectarSqlServer())
            {
                con.Query(query, param);
            }
        }

        /// <summary>
        /// Obtem uma coleção de produtos obtidas do banco de dados.
        /// </summary>
        /// <returns>Coleção de produtos</returns>
        public IEnumerable<Produto> ObterTodos()
        {
            var query = @"SELECT id, nome, descricao, valor, imagem FROM produto";

            using (var con = this.provedorConexao.ConectarSqlServer())
            {
                return con.Query<Produto>(query).Select(produto => new Produto
                {
                    Id = produto.Id,
                    Nome = produto.Nome,
                    Descricao = produto.Descricao,
                    Valor = produto.Valor,
                    Imagem = produto.Imagem,
                });
            }
        }
        /// <summary>
        /// Retorna um unico produto, selecionado pelo seu id, do banco de dados.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Produto</returns>
        public Produto ObterPorId(int id)
        {
            var param = new DynamicParameters();
            param.Add("@Id", id);

            var query = @"SELECT id, nome, descricao, valor, imagem FROM produto WHERE
                          id = @Id";

            using (var con = this.provedorConexao.ConectarSqlServer())
            {
                return con.Query<Produto>(query, param).Select(produto => new Produto
                {
                    Id = produto.Id,
                    Nome = produto.Nome,
                    Descricao = produto.Descricao,
                    Valor = produto.Valor,
                    Imagem = produto.Imagem,
                }).FirstOrDefault();
            }
        }
    }
}