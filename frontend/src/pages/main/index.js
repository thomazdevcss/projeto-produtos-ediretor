import React, { Component } from "react";
import { api, formatarDinheiro } from "../../services";
import swal from "sweetalert";
import ModalProduto from "../../components/modal-produto/index";

import "./style.css";

export default class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      produtos: [],
      produto: null,
      modalAtivo: false
    };

    this.deletarProduto = this.deletarProduto.bind(this);
    this.buscarProdutos = this.buscarProdutos.bind(this);
    this.abrirModal = this.abrirModal.bind(this);
  }

  componentDidMount() {
    this.buscarProdutos();
  }

  buscarProdutos = async () => {
    await api
      .getProdutos()
      .then(res => {
        this.setState({ produtos: res.data });
      })
      .catch(() =>
        swal(
          "Algo deu errado!",
          "Aparentemente o servidor esta fora do ar!",
          "error"
        )
      );
  };

  deletarProduto = async e => {
    const id = e.target.value;
    await api.deleteProduto(id);
    this.buscarProdutos();
  };

  abrirModal = produto => {
    if (produto.id) this.setState({ produto: produto });
    this.setState({ modalAtivo: true });
  };

  fecharModal = () => {
    this.setState({ modalAtivo: false });
    this.setState({ produto: null });
  };

  render() {
    const { produtos, produto } = this.state;

    return (
      <main className="main">
        <div className="corpo-tabela">
          <table className="tabela-produtos">
            <tbody>
              <tr>
                <th>id</th>
                <th>nome</th>
                <th>descrição</th>
                <th>valor</th>
                <th>imagem</th>
                <th className="funcao">Editar</th>
                <th className="funcao">Excluir</th>
              </tr>
              {produtos.map(produto => (
                <tr key={produto.id}>
                  <td className="fade">{produto.id}</td>
                  <td>{produto.nome}</td>
                  <td id="descricao">{produto.descricao}</td>
                  <td>{formatarDinheiro(produto.valor)}</td>
                  <td
                    id="link"
                    onClick={() => window.open(produto.imagem, "_blank")}
                  >
                    <a href="imagem">{produto.imagem}</a>
                  </td>
                  <td>
                    <button
                      id="editar"
                      onClick={() => {
                        this.abrirModal(produto);
                      }}
                    >
                      <img
                        src="https://image.flaticon.com/icons/svg/1159/1159633.svg"
                        alt="editar"
                      />
                    </button>
                  </td>
                  <td>
                    <button
                      id="deletar"
                      onClick={this.deletarProduto}
                      value={produto.id}
                    >
                      deletar
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
        <button
          className="adicionar-btn"
          onClick={this.abrirModal}
          value={"adicionar"}
        >
          Adicionar produto
        </button>

        {this.state.modalAtivo ? (
          <ModalProduto
            fechar={this.fecharModal}
            buscar={this.buscarProdutos}
            produto={produto}
          />
        ) : null}
      </main>
    );
  }
}
