import React, { Component } from "react";
import { api } from "../../services";
import swal from "sweetalert";
import "./style.css";

export default class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      produto: this.props.produto,
      botaoDesabilitado: false,
      nome: "",
      descricao: "",
      imagem: "",
      valor: ""
    };
    this.handleInputChange = this.handleInputChange.bind(this);
  }
  componentDidMount() {
    if (this.props.produto) {
      var produto = this.props.produto;
      this.setState({ nome: produto.nome });
      this.setState({ descricao: produto.descricao });
      this.setState({ imagem: produto.imagem });
      this.setState({ valor: produto.valor });
    }
  }

  inserirProduto = async e => {
    e.preventDefault();
    const produto = {
      nome: this.state.nome,
      descricao: this.state.descricao,
      imagem: this.state.imagem,
      valor: this.state.valor
    };
    if (produto.nome === "" || produto.valor === 0) {
      swal("Algo deu errado!", "Revise os campos por favor!", "error");
      return;
    }
    await api
      .postProduto(produto)
      .then(() => swal("Sucesso!", "Produto foi inserido!", "success"))
      .catch(() =>
        swal("Algo deu errado!", "Revise os campos por favor!", "error")
      );
    this.props.buscar();
    this.state.nome = "";
    this.state.descricao = "";
    this.state.valor = "";
    this.state.imagem = "";
  };

  alterarProduto = async e => {
    e.preventDefault();
    const produto = this.props.produto;
    produto.nome = this.state.nome;
    produto.descricao = this.state.descricao;
    produto.imagem = this.state.imagem;
    produto.valor = this.state.valor;

    await api
      .putProduto(produto.id, produto)
      .then(() => swal("Sucesso!", "Produto foi alterado!", "success"))
      .catch(() =>
        swal("Algo deu errado!", "Revise os campos por favor!", "error")
      );

    this.props.buscar();
    this.props.fechar();
  };

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  render() {
    return (
      <div
        className="modal-background"
        onClick={e => {
          if (e.target === e.currentTarget) this.props.fechar();
        }}
      >
        <div className="modal-produto">
          <button className="fechar-btn" onClick={this.props.fechar}>
            x
          </button>
          <h1>Inserir produto</h1>
          <form className="form-produto">
            <div className="nome">
              <label htmlFor="nome">Nome:</label>
              <input
                type="text"
                id="nome"
                name="nome"
                value={this.state.nome}
                onChange={this.handleInputChange}
              />
            </div>
            <div className="valor">
              <label htmlFor="valor">Valor:</label>
              <input
                type="number"
                id="valor"
                name="valor"
                value={this.state.valor}
                onChange={this.handleInputChange}
              />
            </div>
            <div className="imagem">
              <label htmlFor="imagem">Url da imagem:</label>
              <input
                type="text"
                id="imagem"
                name="imagem"
                value={this.state.imagem}
                onChange={this.handleInputChange}
              />
            </div>
            <div className="descricao">
              <label htmlFor="descricao">descrição:</label>
              <textarea
                id="descricao"
                name="descricao"
                rows="num"
                cols="num"
                value={this.state.descricao}
                onChange={this.handleInputChange}
              ></textarea>
            </div>
            <div className="btn">
              <button
                disabled={this.state.botaoHabilitado}
                onClick={
                  this.props.produto ? this.alterarProduto : this.inserirProduto
                }
              >
                Enviar
              </button>
            </div>
          </form>
        </div>
      </div>
    );
  }
}
