using System.Collections.Generic;
using backend.Dominio.Entidade;

namespace backend.Dominio.Servico.Interface
{
    public interface IServicoProduto
    {
        void Inserir(Produto produto);
        void Alterar(int id, Produto produto);
        void Deletar(int id);
        IEnumerable<Produto> ObterTodos();
        Produto ObterPorId(int id);
    }
}