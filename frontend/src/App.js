import React from "react";
import Header from "./components/header/index";
import Main from "./pages/main/index";

function App() {
  return (
    <div className="App">
      <Header />
      <Main />
    </div>
  );
}

export default App;
