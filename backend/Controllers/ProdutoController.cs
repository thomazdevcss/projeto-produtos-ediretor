﻿using System.Collections.Generic;
using backend.Dominio.Entidade;
using backend.Dominio.Servico.Interface;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProdutoController : ControllerBase
    {
        private IConfiguration configuration;
        private IServicoProduto ServicoProduto;
        public ProdutoController(IConfiguration configuration, IServicoProduto ServicoProduto)
        {
            this.configuration = configuration;
            this.ServicoProduto = ServicoProduto;
        }
        /// <summary>
        ///  Solícita ao servico de produto uma Coleção de produtos.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<IEnumerable<Produto>> Get()
        {
            return new OkObjectResult(this.ServicoProduto.ObterTodos());
        }
        /// <summary>
        /// Solícita ao servico de produto um unico produto
        /// especificado pelo seu id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>

        [HttpGet("{id}")]
        public ActionResult<Produto> Get(int id)
        {
            return new OkObjectResult(this.ServicoProduto.ObterPorId(id));
        }
        /// <summary>
        /// Solícita ao servico de produto para que seja inserido um registro 
        /// de novo produto.
        /// </summary>
        /// <param name="produto"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Post([FromBody] Produto produto)
        {
            this.ServicoProduto.Inserir(produto);
            return new CreatedResult("Inserido com sucesso!", produto);
        }
        /// <summary>
        /// Solícita ao servico de produto para ser feita uma 
        /// alteracão neste produto especificado pelo seu id.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="produto"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public ActionResult Put(int id, [FromBody] Produto produto)
        {
            this.ServicoProduto.Alterar(id, produto);
            return new CreatedResult("Alterado com sucesso!", produto);
        }
        /// <summary>
        /// Solícita ao servico de produto para que seja deletado esse produto 
        /// especificado pelo seu id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            this.ServicoProduto.Deletar(id);
            return new OkResult();
        }
    }
}
