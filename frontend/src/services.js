import axios from "axios";

const http = axios.create({
  baseURL: "https://localhost:5001/api"
});

export const api = {
  getProdutos() {
    return http.get("/produto");
  },
  getProdutoPorId(id) {
    return http.get(`/produto/${id}`);
  },
  postProduto(produto) {
    return http.post("/produto", produto);
  },
  putProduto(id, produto) {
    return http.put(`/produto/${id}`, produto);
  },
  deleteProduto(id) {
    return http.delete(`/produto/${id}`);
  }
};

export const formatarDinheiro = valor => {
  if (Number.isNaN(valor)) return;

  return valor.toLocaleString("pt-BR", { style: "currency", currency: "BRL" });
};
