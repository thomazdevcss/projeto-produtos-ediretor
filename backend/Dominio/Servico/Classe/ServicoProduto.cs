using System.Collections.Generic;
using backend.Dominio.Entidade;
using backend.Dominio.Repositorio.Interface;
using backend.Dominio.Servico.Interface;
using Microsoft.Extensions.Configuration;
using System;

namespace backend.Dominio.Servico.Classe
{
    public class ServicoProduto : IServicoProduto
    {
        private IConfiguration configuration;
        private IRepositorioProduto repositorioProduto;
        public ServicoProduto(IConfiguration configuration, IRepositorioProduto repositorioProduto)
        {
            this.configuration = configuration;
            this.repositorioProduto = repositorioProduto;
        }
        /// <summary>
        /// Solícita ao repositório de produto para ser feita uma 
        /// alteracão neste produto especificado pelo seu id.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="produto"></param>
        public void Alterar(int id, Produto produto)
        {
            this.repositorioProduto.Alterar(id, produto);
        }
        /// <summary>
        /// Solícita ao repositório de produto para que seja deletado esse produto 
        /// especificado pelo seu id.
        /// </summary>
        /// <param name="id"></param>
        public void Deletar(int id)
        {
            this.repositorioProduto.Deletar(id);
        }
        /// <summary>
        /// Solícita ao repositório de produto para que seja inserido um registro 
        /// de novo produto.
        /// </summary>
        /// <param name="produto"></param>
        public void Inserir(Produto produto)
        {
            this.repositorioProduto.Inserir(produto);
        }
        /// <summary>
        /// Solícita ao repositório de produto um unico produto
        /// especificado pelo seu id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Produto</returns>
        public Produto ObterPorId(int id)
        {
            return this.repositorioProduto.ObterPorId(id);
        }
        /// <summary>
        /// Solícita ao repositório de produto uma coleção de produtos.
        /// </summary>
        /// <returns>Coleção de produtos</returns>
        public IEnumerable<Produto> ObterTodos()
        {
            return this.repositorioProduto.ObterTodos();
        }
    }
}