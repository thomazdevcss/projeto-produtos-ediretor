using System.Data;

namespace backend.Dominio.Repositorio.Interface
{
    public interface IProvedorConexao
    {
        IDbConnection ConectarSqlServer();
    }
}