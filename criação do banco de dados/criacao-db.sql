CREATE DATABASE sistema_produtos
GO

USE sistema_produtos;

CREATE TABLE produto (
id INT IDENTITY(1,1) PRIMARY KEY,
nome VARCHAR(100),
descricao VARCHAR(MAX),
valor FLOAT,
imagem VARCHAR(MAX)
);