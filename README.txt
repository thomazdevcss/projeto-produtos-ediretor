Projeto Ediretor - Produtos

Visão geral

Aplicacão para controle e consulta de produtos em estoque.

--

Back-end

API escrita em C# usando a SDK .NET CORE WEBAPI versão 2.204.

A conexão com o banco de dados é feita atraves do ORM Dapper.

O arquivo para criação do banco de dados se encontra na pasta "criação do banco de dados.

documentação da API disponivel no documento "documentacao-api" na pasta de arquitetura (consulta-lo para informacões e exemplos sobre a API)

Execucão: abra o diretorio do back-end em seu terminal de comando e utilize o comando "dotnet run" 
e aguarde a execucão da aplicão.

--

Front-end

Escrito em React.js utilizando a CLI create-react-app.

Conexão com a API é feita com o auxilio do framework Axios.

Cliente responsivo que se adequa ao comprimento da tela.

Ter o node.js instalado em sua maquina e obrigatorio para a execucão do servidor do cliente.

Execucão:abra o diretorio do front-end em seu terminal e utilize o comando "npm start"
e aguarde a execucão da aplicacão.


INFORMAÇÕES GERAIS

Criador - Thomaz Carvalho

Contato - thomazdevcss@gmail.com