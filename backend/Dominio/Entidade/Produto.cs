namespace backend.Dominio.Entidade
{
    /// <summary>
    /// Modelo de produto do sistema
    /// </summary>
    public class Produto
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public string Imagem { get; set; }
        public double Valor { get; set; }
    }
}