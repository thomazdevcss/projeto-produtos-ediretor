using System.Data;
using backend.Dominio.Repositorio.Interface;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;

namespace backend.Dominio.Repositorio.Classe
{
    public class ProvedorConexao : IProvedorConexao
    {
        private IConfiguration configuration;
        private string strConexao = "SistemaProdutosConexaoSqlServer";

        public ProvedorConexao(IConfiguration configuration)
        {
            this.configuration = configuration;
        }
        /// <summary>
        /// Metodo que fornece conexão ao banco de dados
        /// </summary>
        /// <returns>SqlConnection</returns>
        public IDbConnection ConectarSqlServer()
        {
            return new SqlConnection(configuration.GetConnectionString(strConexao));
        }
    }
}